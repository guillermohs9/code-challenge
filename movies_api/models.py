from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import Avg



class Movie(models.Model):
    title = models.CharField(max_length = 200, null=False, blank=False)
    plot = models.TextField(null=False, blank=False)
    genre = models.CharField(max_length = 50, null=False, blank=False)
    release_year = models.IntegerField(null=False, blank=False)
    date_added = models.DateTimeField(auto_now_add = True)
    poster_url = models.CharField(max_length = 200, null=True, blank=True)

    
    def __str__(self):
        return self.title
    
    def get_avg_rating(self):
        ratings_set = self.ratings.all()
        count = 0
        sum = 0
        for i in ratings_set.iterator():
            sum += i.rate
            count += 1
        if count > 0:
            return (sum/count)
        else:
            return 0



class Rating(models.Model):
    rate = models.IntegerField(null=False, blank=False, validators=[MinValueValidator(0), MaxValueValidator(5)])
    comment = models.TextField(null=False, blank=False)
    date_added = models.DateTimeField(auto_now_add = True)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='ratings')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_ratings')
