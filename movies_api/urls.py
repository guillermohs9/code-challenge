from django.urls import path
from .views import MovieList, MovieDetail, UserRegister


app_name = 'movies_api'

urlpatterns = [
    path('<int:pk>/', MovieDetail.as_view()),
    path('', MovieList.as_view(), name='movies_list'),
    path('register/', UserRegister.as_view())
]


