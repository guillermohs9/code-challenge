from rest_framework import generics
from .models import Movie
from .serializers import MovieSerializer, RatingSerializer, UserSerializer
from django.db.models import Avg
from rest_framework import permissions
from django.contrib.auth.models import User


class MovieList(generics.ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class MovieDetail(generics.RetrieveAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    
    
class UserRegister(generics.CreateAPIView):
    model = User
    permission_classes = [
        permissions.AllowAny # Or anon users can't register
    ]
    serializer_class = UserSerializer