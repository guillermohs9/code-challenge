from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status


class TestListRegisterUser(APITestCase):
    
    def authenticate(self):
        self.client.post(reverse('register_user'), 
                    {"username": "test_username", 
                     "password": "test_password", 
                     "email": "test@mail.com"})
        
        response = self.client.post(reverse('token_obtain_pair'), 
                                    {"username": "test_username", 
                                     "password": "test_password"})
        
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {response.data['access_token']}")
    
    def test_register_user(self):
        sample_user = {"username": "test_username", 
                       "password": "test_password", 
                       "email": "test@mail.com"}
        response = self.client.post(reverse('register_user'), sample_user)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
    def test_should_not_add_movie(self):
        sample_movie = {"title": "title",
                        "plot": "plot",
                        "genre": "genre",
                        "release_year": 2022}
        response = self.client.post(reverse('movies_api:movies_list'), sample_movie)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
    def test_should_add_movie(self):
        self.authenticate()
        sample_movie = {"title": "title",
                        "plot": "plot",
                        "genre": "genre",
                        "release_year": 2022}
        response = self.client.post(reverse('movies_api:movies_list'), sample_movie)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

