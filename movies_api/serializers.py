from rest_framework import serializers
from .models import Movie, Rating
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user
        

class RatingSerializer(serializers.ModelSerializer):
    def get_username(self, obj):
        return obj.user.username
    username = serializers.SerializerMethodField("get_username")
    class Meta:
        model = Rating
        fields = ('id', 'rate', 'comment', 'date_added', 'username')


class MovieSerializer(serializers.ModelSerializer):
    ratings = RatingSerializer(read_only=True, many=True)
    avg_rate_field = serializers.FloatField(source='get_avg_rating', read_only=True)
    class Meta:
        model = Movie
        fields = ('id', 'title', 'plot', 'genre', 'release_year', 'date_added', 'poster_url', 'avg_rate_field', 'ratings')
        
        

        
