import * as React from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { styled } from '@mui/material/styles';
import { Rating } from '@mui/material';
import { TextField } from '@mui/material';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import moment from 'moment'



const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
});


export const MovieDetails = () => {
  const navigate = useNavigate();

  const [value, setValue] = useState(0);
  const [title, setTitle] = useState('');
  const { id } = useParams();
  const [movie, setMovie] = useState({});

  useEffect(() => {
      fetch(`/api/${id}`)
          .then(response => response.json())
          .then(data => setMovie(data))
          .catch(err => {
              console.log(err);
          })
  }, [])
  const token = localStorage.getItem('access_token');
  return (
    <div>
    <Container sx={{ pt: 5 }} maxWidth="md">
      <Grid container spacing={2}>
        <Grid container>
          <Grid item xs={3}>
            <Box sx={{ width: 180, height: 180 }}>
              <Img alt="movie_poster" src={movie.poster_url ? "https://image.tmdb.org/t/p/w154" + movie.poster_url : "https://dummyimage.com/140/bbb/fff.png&text=++++POSTER+++"} />
            </Box>
          </Grid>
          <Grid item xs={7} container rowSpacing={2}>
            <Grid item xs={12}>
              <Typography variant="h3">
                {movie.title}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h5">
                Release date: {movie.release_year}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                Genre: {movie.genre}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                Plot: {movie.plot}
              </Typography>
            </Grid>
            <Grid item xs={12} container>
              {/* <Typography fontSize={16}>
                Average Rating: 
                <Rating name="read-only" value={movie.ratings.length > 0 && movie.avg_rate_field} precision={0.5} readOnly />
                {movie.ratings.length > 0 ? ` ${movie.avg_rate_field}/5` : 'No ratings yet!'}
              </Typography> */}
            </Grid>
          </Grid>
          {
            token !== null && (
              <Grid item xs={2} container rowSpacing={2}>
                <Grid item xs={12}>
                  <Button onClick={() => {navigate(`/edit/${movie.id}`)}} sx = {{ my: 2 }} variant="contained">
                    Edit info
                  </Button>
                  <Button sx = {{ my: 2 }} variant="contained" color="error">
                    Delete movie
                  </Button>
                </Grid>
              </Grid>
            )
          }

          <Grid item xs={12}>
            <Typography sx={{ mt: 5 }} variant="h6">
              Users' ratings:
            </Typography>            
          </Grid>
          <Grid sx={{ mt: 4 }} item xs={12} container rowSpacing={2}>
            {/* {movie.ratings.map(rating => {
              return (
                [<Grid key={rating.id} item xs={2}>
                  <Rating name="read-only" value={rating.rate} precision={0.5} readOnly />
                </Grid>, 
                <Grid item xs={10}>
                  <Typography variant="body2" color="text.secondary">
                    Review by {rating.username} on {moment(rating.date_added).format('MMM Do YYYY')}
                  </Typography>
                  <Typography>
                    {rating.comment}
                  </Typography>
                </Grid>]
              );
            })} */}
          </Grid>
        </Grid>
      </Grid>
    </Container>
    {
      token !== null && (
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
        <Typography component="h1" variant="h5">
          Add your review
        </Typography>
        <Box component="form" noValidate sx={{ mt: 1 }}>
          <Rating
            name="simple-controlled"
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
            }}
          />
          <TextField
            required
            fullWidth
            id="comment"
            label="Your review"
            defaultValue=""
          />
          <Button
            type="submit"
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Send review
          </Button>
        </Box>
      </Box>
      )
    }
    </div>
)
}