import * as React from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { styled } from '@mui/material/styles';
import { Rating } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';

const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
});


export const Home = () => {
  const navigate = useNavigate()
  const [movies, setMovies] = useState([]);

  useEffect(() => {
      fetch('/api/')
          .then(response => response.json())
          .then(data => setMovies(data))
          .catch(err => {
              console.log(err);
          })
  }, [])
  return (
    <Container sx={{ pt: 5 }} maxWidth="md">
      <Grid container spacing={2}>
        {movies.map(movie => {
          return (
            <Grid key={movie.id} item xs={12} container>
              <Grid item>
                <Box sx={{ width: 180, height: 180 }}>
                  <Img alt="movie_poster" src={movie.poster_url ? "https://image.tmdb.org/t/p/w154" + movie.poster_url : "https://dummyimage.com/140/bbb/fff.png&text=++++POSTER+++"} />
                </Box>
              </Grid>
              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={2}>
                  <Grid item xs>
                    <Typography  fontSize={16} gutterBottom variant="subtitle1" component="div">
                      {movie.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {movie.genre}
                    </Typography>
                    <Typography variant="body2" gutterBottom>
                      {movie.plot}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Grid container>
                      <Grid xs={3} item>
                        <Rating name="read-only" value={movie.ratings.length > 0 && movie.avg_rate_field} precision={0.5} readOnly />
                      </Grid>
                      <Grid xs={3} item>
                        <Typography variant="body2" color="text.secondary">
                          {movie.ratings.length > 0 ? `Rating: ${movie.avg_rate_field}/5` : 'No ratings yet!'}
                        </Typography>
                      </Grid>
                      <Grid sx={{ mx: 'auto', textAlign: 'center' }} xs={6} item>
                        <Button onClick={() => navigate(`/details/${movie.id}`)} variant="contained">
                          View Details
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle1" component="div">
                    {movie.release_year}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </Container>
  )
}