import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Home } from './pages/Home';
import { SignUp } from './pages/SignUp';
import { SignIn } from './pages/SignIn';
import { ButtonAppBar } from './components/AppBar';
import { MovieDetails } from './pages/MovieDetails';
import { PostForm } from './components/PostForm';


function App() {
  return (
    <Router>
      <ButtonAppBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/register" element={<SignUp />} />
        <Route path="/signin" element={<SignIn />} />
        <Route path="/add" element={<PostForm />} />
        <Route path="/edit/:id" element={<PostForm />} />
        <Route path="/details/:id" element={<MovieDetails />} />
      </Routes>
    </Router>

  );
}

export default App;
