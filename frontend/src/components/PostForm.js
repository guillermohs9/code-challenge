import { useState } from "react";
import Button from '@mui/material/Button';
import { TextField, Container, Box } from "@mui/material";
import { useNavigate } from 'react-router-dom'
import { axiosObject } from "../Axios_fr";
import Typography from '@mui/material/Typography';
import { useParams } from "react-router-dom";


export const PostForm = () => {
  const navigate = useNavigate();

  const [title, setTitle] = useState('')
  const [plot, setPlot] = useState('')
  const [genre, setGenre] = useState('')
  const [release_year, setReleaseYear] = useState('')

  const { id } = useParams();

  if (id) {
    console.log('edit')
  } else {
    console.log('new')
  }

  var posterUrl = "";
  function fetchPoster(title) {
    fetch(`https://api.themoviedb.org/3/search/movie?query=${title}&api_key=2bc5f718b753acb378f39e23edb2fb1f`)
    .then(res => res.json())
    .then(data => posterUrl = data)
    .then(() => console.log(posterUrl))
  }

  const submitHandler = (event) => {
    event.preventDefault()
    fetchPoster(title)
    let results = posterUrl.results
    let poster_path
    results.length > 0 ? poster_path = results[0].poster_path : console.log('no poster image found');
    axiosObject
      .post(`/api/`, {
        title: title,
        plot: plot,
        genre: genre,
        release_year: release_year,
        poster_url: poster_path,
      })
      .then((res) => {
        console.log(res)
        navigate('/')
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log('Error', error.message);
        }
        console.log(error.config);
      });
  }

  
  return (
    <Container maxWidth="xs">
      <Box component="form" onSubmit={submitHandler} 
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography sx={{ my: 1 }} component="h1" variant="h5">
          {(id) ? 'Edit a movie' : 'Add a Movie'}
        </Typography>
        <TextField 
          fullWidth 
          sx={{ my: 1 }} 
          onChange={(e) => setTitle(e.target.value)} 
          value={title} 
          id="title" 
          label="Title" 
        />
        <TextField 
          rows={4} 
          multiline 
          fullWidth 
          sx={{ my: 1 }} 
          onChange={(e) => setPlot(e.target.value)} 
          value={plot} 
          id="plot" 
          label="Plot" 
        />
        <TextField 
          sx={{ my: 1 }} 
          fullWidth 
          onChange={(e) => setGenre(e.target.value)} 
          value={genre} 
          id="genre" 
          label="Genre" 
        />
        <TextField 
          inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }} 
          sx={{ my: 1 }} 
          fullWidth 
          onChange={(e) => setReleaseYear(e.target.value)} 
          value={release_year} 
          id="release_year" 
          label="Release Year" 
        />
        <Button sx={{ my: 3 }} fullWidth variant="contained" type='submit'>
          Submit
        </Button>
      </Box>
    </Container>

  );
}