import axios from 'axios';

const baseUrl = ''


export const axiosObject = axios.create({
    baseURL: baseUrl,
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
        accept: 'application/json',
    },
});

axiosObject.interceptors.request.use(function (config) {
    let token = localStorage.getItem("access_token");
    if (token) {
        config.headers["Authorization"] = "Bearer " + token;
    } else {
        config.headers["Authorization"] = null;
    }

    return config;
  });


